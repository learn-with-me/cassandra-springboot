package com.springboot.cassandra.service;

import com.springboot.cassandra.model.Book;
import com.springboot.cassandra.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BookService {

    private BookRepository repository;

    @Autowired
    public BookService(BookRepository repository) {
        this.repository = repository;
    }

    public Book addBook(Book book) {
        return repository.save(book);
    }

    public List<Book> getAllBooks() {
        return repository.findAll();
    }

    public Book getBooksByTitleAndPublisher(String title, String publisher, UUID id) {
        return repository.findByTitleAndPublisherAndId(title, publisher,id);
    }

}
