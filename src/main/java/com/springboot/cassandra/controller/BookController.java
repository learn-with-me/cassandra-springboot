package com.springboot.cassandra.controller;

import com.springboot.cassandra.model.Book;
import com.springboot.cassandra.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/books")
public class BookController {

    private BookService service;

    @Autowired
    public BookController(BookService service) {
        this.service = service;
    }

    @GetMapping
    public List<Book> getAllBooks() {
        return service.getAllBooks();
    }

    @GetMapping("/{title}/{publisher}/{id}")
    public Book getBookById(@PathVariable String title, @PathVariable String publisher, @PathVariable UUID id) {
        return service.getBooksByTitleAndPublisher(title, publisher, id);
    }

    @PostMapping
    public Book addBook(@RequestBody Book book) {
        Set<String> tags = new HashSet<>();
        tags.add("springboot");
        tags.add("cassandra");
        book.setTags(tags);
        return service.addBook(new Book(book.getTitle(), book.getPublisher(), tags));
    }

}
