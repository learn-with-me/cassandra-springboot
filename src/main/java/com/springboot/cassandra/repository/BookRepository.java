package com.springboot.cassandra.repository;

import com.springboot.cassandra.model.Book;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BookRepository extends CassandraRepository<Book, UUID> {

    @Query("select * from book where title = ?0 and publisher=?1 and id=?2")
    Book findByTitleAndPublisherAndId(String title, String publisher, UUID id);

}