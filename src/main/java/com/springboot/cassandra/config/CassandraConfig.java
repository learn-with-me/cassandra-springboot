package com.springboot.cassandra.config;

import com.datastax.driver.core.ProtocolVersion;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.KeyspaceActions;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.*;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.data.cassandra.util.MapBuilder;

import java.util.Arrays;
import java.util.List;

@Configuration
@PropertySource(value = { "classpath:application.properties" })
@EnableCassandraRepositories(basePackages = "com.springboot.cassandra.repository")
public class CassandraConfig extends AbstractCassandraConfiguration {

    private static final Log LOGGER = LogFactory.getLog(CassandraConfig.class);
    public static final String KEYSPACE = "spring_boot";

    private Environment environment;

    @Autowired
    public CassandraConfig(Environment environment) {
        this.environment = environment;
    }

    @Override
    protected boolean getMetricsEnabled() { return false; }

    @Override
    protected String getKeyspaceName() {
        return KEYSPACE;
    }

    @Override
    @Bean
    public CassandraClusterFactoryBean cluster() {
        final CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
        cluster.setJmxReportingEnabled(getMetricsEnabled());
        cluster.setContactPoints("127.0.0.1");
        cluster.setProtocolVersion(ProtocolVersion.V4);
        cluster.setPort(Integer.parseInt("9042"));
        cluster.setKeyspaceActions(keyspaceActions());
        cluster.setKeyspaceDrops(getKeyspaceDrops());
        LOGGER.info("Cluster created with contact points [" + environment.getProperty("cassandra.contactpoints") + "] " + "& port [" + Integer.parseInt(environment.getProperty("cassandra.port")) + "].");
        return cluster;
    }

    public List<KeyspaceActions> keyspaceActions() {

        KeyspaceActionSpecification specification =
                CreateKeyspaceSpecification.createKeyspace(getKeyspaceName())
                        .ifNotExists()
                        .with(KeyspaceOption.REPLICATION,
                                getReplicationOption());

        KeyspaceActions actions = new KeyspaceActions(specification);

        return List.of(actions);
    }

    private MapBuilder<Option, Object> getReplicationOption() {
        return MapBuilder.map(Option.class, Object.class)
                .entry(new DefaultOption("class", String.class, true, false, true)
                        , KeyspaceOption.ReplicationStrategy.SIMPLE_STRATEGY.getValue())
                .entry(new DefaultOption("replication_factor", Long.class, true, false, false)
                        , 3);
    }

    @Override
    public SchemaAction getSchemaAction() {
        return SchemaAction.CREATE_IF_NOT_EXISTS;
    }

    @Override
    protected List<DropKeyspaceSpecification> getKeyspaceDrops() {
        return Arrays.asList(DropKeyspaceSpecification.dropKeyspace(KEYSPACE));
    }

}
