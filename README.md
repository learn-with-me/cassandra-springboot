# README #

This README would normally document all steps need to run the application

### What is this repository for? ###

* Spring Boot Application which integrates with Cassandra. This is a simple application to demonstrate cassandra usage in spring boot app.
* Cassandra Version 3 with spring boot parent version 2.2.5.RELEASE  
* [Way To Repository](https://bitbucket.org/KumarDhiren/cassandra-springboot/src/master/)

### Local SetUP for Cassandra using Docker ###

>
    Docker Commands:
    -------------------------------------------------------------------
    docker run --name local-cassandra -p 9042:9042 -d cassandra
    docker exec -it local-cassandra cqlsh

>
    SQL Scripts to create Keyspace and Table in Cassandra
    ------------------------------------------------------------------
    CREATE KEYSPACE spring_boot WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 3};
    USE spring_boot;
    CREATE TABLE book(id UUID, title text, publisher text, tags SET<text>,PRIMARY KEY(id, title, publisher));

    - In our application keyspace creation is not required as we are doing it from the application
    - But we need to create table before running the application.

### How do I get set up? ###
>
    * Summary of set up
    * Configuration
    * Dependencies
    * Database configuration
    * How to run tests
    * Deployment instructions
>

#### Dependencies ####

>
    - Spring Initializer with dependencies as 
    1) spring-data-jpa
    2) spring-data-cassandra / spring-boot-starter-data-cassandra
    3) spring-boot-starter-web
    4) lombok
>

### Database Configuration ###

> cassandra.contactpoints=127.0.0.1

- This is used to mention the IP connect to the cassandra host 

> cassandra.port=9042

- Port where the host is running. 9042 is the default port

>  cassandra.keyspace=springboot_cass

- Keyspace to connect in our application

>  spring.data.cassandra.connect-timeout=10000ms

- Connect timeout for cassandra

>  spring.data.cassandra.read-timeout=10000ms

- Read timeout for cassandra

>  spring.data.cassandra.pool.pool-timeout=10000ms


### Who do to contact ? ###

* Repo owner or admin
* Other community or team contact